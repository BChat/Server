<div align="center">
  <h1>BChat server version</h1>
  <p>
    <strong>
      Modern and secure internet chat.
    </strong>
  </p>
  <p>
   Better Chat is BChat
  </p>

[![Release](https://img.shields.io/badge/release-none-red)](https://codeberg.org/BChat/Server/releases)
[![License](https://img.shields.io/badge/GPL-3.0_only-green)](https://opensource.org/licenses/GPL-3.0)
[![Version](https://img.shields.io/badge/rustc-nightly+-ab6000.svg)]()
<br />
[![Chat on Matrix](https://img.shields.io/badge/chat-on_matrix-51bb9c)](https://matrix.to/#/#bchat:matrix.org)
  
</div>

## Table of content

- [About](#about)
  - [Chat modes](#chat-modes)
- [Assumptions](#assumptions)
- [Screenshots](#screenshots)
- [Operating systems](#operating-systems)
- [Built with](#built-with)
  - [Building](#building)
- [TODO](#todo)
- [Donation](#donation)
  - [Cryptocurrencies](#cryptocurrencies)
- [Licensing](#licensing)

# About

> About the project

<strong>The project aims to create a place for a secure online communication.</strong>

What does BChat mean? This means Better Chat.

Because it should be better than the competitive by providing better security,
good encryption (E2EE) and the possibility to meet new interesting
people without registration.

## Chat modes

- Conversation with a random person, one on one
- Writing on a group chat
- Random group of several people
- Providing a disposable url to chatting
- Conversation with a random person with a countdown to hang up
- Chat with a star (verified) with a timer to disconnect

There are a lot of different chats on the internet, which without the need to
register an account allows to write with random people to get to know each other.
But they all do not have what is in the assumptions of BChat.

# Assumptions

- Free and open-source
- Without profiling users and collecting statistics
- Strong security and E2EE
- Newest technologies
- Modern and variable graphic look
- A lot of interesting possibilities such as, for example: random jokes,
  sharing the screen, playing in a simple games (eg. snake, chess)
- Conducting calls via the webcam

# Screenshots

# Operating systems

> Chat tests on operating systems

| Server operating system | Tested | Works |
| ----------------------- | ------ | ----- |
| Alpine Linux            | Yes    | Yes   |

| Developer operating system | Tested | Works |
| -------------------------- | ------ | ----- |
| Debian Bullseye            | Yes    | Yes   |

# Built with

- Rust nightly
- Dart 2.17.6
- Dart SASS (as Dart library)

## Building

> How to build

```
git clone https://codeberg.org/BChat/Server
cargo build --release
```

# TODO

# Donation

## Cryptocurrencies

- Pirate Chain (ARRR):

  >

- Beam:

  >

- Monero (XMR):

  >

- Dash:

  >

- Bitcoin Cash (BCH):

  >

- Ethereum (ETH):

  >

- Dogecoin (DOGE):

  >

- Litecoin (LTC):

  >

- Bitcoin (BTC):
  >

# Licensing

GPL-3.0 only
