#[macro_use]
extern crate rocket;

mod chat;
use chat::group::GroupMessage;
mod templates;

use rocket::fs::{relative, FileServer};
use rocket::tokio::sync::broadcast::channel;
use rocket_dyn_templates::Template;

/// Main
#[launch]
fn rocket() -> _ {
    rocket::build()
        // Templates
        .mount(
            "/",
            routes![
                templates::tera::index,
                templates::tera::about,
                templates::tera::chat,
                templates::tera::chat_group_random,
                templates::tera::chat_group,
                templates::tera::chat_random,
                templates::tera::chat_random_countdown,
                templates::tera::chat_via_link,
                templates::tera::chat_star_countdown,
                templates::tera::donation,
                templates::tera::faq,
                templates::tera::how_to,
                templates::tera::social,
                templates::tera::testing, /*templates::tera::example*/
            ],
        )
        //.mount("/hbs", routes![templates::hbs::example])
        //.mount("/askama", routes![templates::askama::example])
        //.register("/askama", catchers![templates::askama::not_found])
        //.register("/hbs", catchers![templates::hbs::not_found])
        .register("/", catchers![templates::tera::not_found])
        .attach(Template::custom(|engines| {}))
        // Chats
        .manage(channel::<GroupMessage>(1024).0)
        .mount(
            "/chat/group",
            routes![chat::group::post, chat::group::events],
        )
        // Files
        .mount("/", FileServer::from(relative!("static")))
    // Others
}
