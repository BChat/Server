use super::rocket;

use rocket::http::{Method::*, RawStr, Status};
use rocket::local::blocking::Client;
use rocket_dyn_templates::{context, Template};

///
fn test_root(kind: &str) {
    // Check that other request methods are not accepted (and instead caught).
    for method in &[Post, Put, Delete, Options, Trace, Connect, Patch] {
        let context = context! { uri: format!("/{}", kind) };
        let expected = Template::show(client.rocket(), format!("{}/error/404", kind), &context);

        let response = client.req(*method, format!("/{}", kind)).dispatch();
        assert_eq!(response.status(), Status::NotFound);
        assert_eq!(response.into_string(), expected);
    }
}

///
fn test_404(base: &str) {
    // Check that the error catcher works.
    let client = Client::tracked(rocket()).unwrap();
    for bad_path in &["/hello", "/foo/bar", "/404"] {
        let path = format!("/{}{}", base, bad_path);
        let escaped_path = RawStr::new(&path).html_escape();

        let response = client.get(&path).dispatch();
        assert_eq!(response.status(), Status::NotFound);
        let response = dbg!(response.into_string().unwrap());

        assert!(response.contains(base));
        assert! {
            response.contains(&format!("{} does not exist", path))
                || response.contains(&format!("{} does not exist", escaped_path))
        };
    }
}

///
fn test_about(base: &str) {
    let client = Client::tracked(rocket()).unwrap();
    let response = client.get(format!("/{}/about", base)).dispatch();
    assert!(response
        .into_string()
        .unwrap()
        //.contains("About"));
}

///
#[test]
fn test_index() {
    let client = Client::tracked(rocket()).unwrap();
    let response = client.get("/").dispatch().into_string().unwrap();
    //assert!(response.contains("Tera"));
    //assert!(response.contains("Handlebars"));
}

///
#[test]
fn hbs() {
    test_root("hbs");
    test_404("hbs");
    test_about("hbs");
}

///
#[test]
fn tera() {
    test_root("tera");
    test_404("tera");
    test_about("tera");
}
