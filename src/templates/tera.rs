use rocket::response::Redirect;
use rocket::Request;

use rocket_dyn_templates::{context, tera::Tera, Template};

#[get("/")]
pub fn index() -> Template {
    Template::render(
        "tera/index",
        context! {
            title: "BChat",
        },
    )
}

#[get("/about")]
pub fn about() -> Template {
    Template::render(
        "tera/pages/about",
        context! {
            title: "About",
        },
    )
}

#[get("/chat")]
pub fn chat() -> Template {
    Template::render(
        "tera/pages/chat/chat",
        context! {
            title: "Chat",
        },
    )
}

#[get("/chat/group_random")]
pub fn chat_group_random() -> Template {
    Template::render(
        "tera/pages/chat/group_random",
        context! {
            title: "Group random chat",
        },
    )
}

#[get("/chat/group")]
pub fn chat_group() -> Template {
    Template::render(
        "tera/pages/chat/group",
        context! {
            title: "Group chat",
        },
    )
}

#[get("/chat/random")]
pub fn chat_random() -> Template {
    Template::render(
        "tera/pages/chat/random",
        context! {
            title: "Random chat",
        },
    )
}

#[get("/chat/random_countdown")]
pub fn chat_random_countdown() -> Template {
    Template::render(
        "tera/pages/chat/random_countdown",
        context! {
            title: "Random chat with countdown",
        },
    )
}

#[get("/chat/via_link")]
pub fn chat_via_link() -> Template {
    Template::render(
        "tera/pages/chat/via_link",
        context! {
            title: "Chat via link",
        },
    )
}

#[get("/chat/star_countdown")]
pub fn chat_star_countdown() -> Template {
    Template::render(
        "tera/pages/chat/star_countdown",
        context! {
            title: "Chat with star",
        },
    )
}

#[get("/donation")]
pub fn donation() -> Template {
    Template::render(
        "tera/pages/donation",
        context! {
            title: "Donation",
        },
    )
}

#[get("/faq")]
pub fn faq() -> Template {
    Template::render(
        "tera/pages/faq",
        context! {
            title: "FAQ",
        },
    )
}

#[get("/how_to")]
pub fn how_to() -> Template {
    Template::render(
        "tera/pages/how_to",
        context! {
            title: "How to",
        },
    )
}

#[get("/social")]
pub fn social() -> Template {
    Template::render(
        "tera/pages/social",
        context! {
            title: "Social",
        },
    )
}

#[get("/testing")]
pub fn testing() -> Template {
    Template::render(
        "tera/testing/testing",
        context! {
            title: "Testing",
        },
    )
}

#[get("/example")]
pub fn example() -> Template {
    Template::render(
        "tera/example/index",
        context! {
            title: "Tera template example",
        },
    )
}

#[catch(404)]
pub fn not_found(req: &Request<'_>) -> Template {
    Template::render(
        "tera/error/404",
        context! {
            uri: req.uri()
        },
    )
}
