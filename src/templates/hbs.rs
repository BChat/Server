use rocket::response::Redirect;
use rocket::Request;

use rocket_dyn_templates::{context, handlebars, Template};

use self::handlebars::{Handlebars, JsonRender};

#[get("/example")]
pub fn example() -> Template {
    Template::render(
        "hbs/example/index",
        context! {
            title: "Handlebars template example",
            parent: "hbs/layout",
        },
    )
}

#[catch(404)]
pub fn not_found(req: &Request<'_>) -> Template {
    Template::render(
        "hbs/error/404",
        context! {
            uri: req.uri()
        },
    )
}
