terraform {
  required_providers {
    vultr = {
      source = "vultr/vultr"
      version = "2.11.4"
    }
  }
}

provider "vultr" {
  # Configuration options
  api_key = var.Vultr_API
  rate_limit = 700
  retry_limit = 4
}