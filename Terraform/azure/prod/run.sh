#!/bin/bash

terraform init

terraform plan -var-file="terraform.tfvars"

terraform apply -var-file="terraform.tfvars" -backup="terraform.tfstate.backup"

terraform graph