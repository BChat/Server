terraform {
  required_providers {
    ovh = {
      source = "ovh/ovh"
      version = "0.23.0"
    }
  }
}

provider "ovh" {
  # Configuration options
}